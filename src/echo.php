#!/usr/bin/env php
<?php

    declare(strict_types=1);

    error_reporting(E_ALL ^ E_WARNING);
    pcntl_async_signals(true);
    pcntl_signal(SIGINT, function () {
        echo Status::Stopped->value . PHP_EOL;
        exit(EXIT_OK);
    });
    const EXIT_WITH_ERROR = 1;
    const EXIT_OK = 0;

    enum Status: String
    {
        case Started = "Server has started. Press Ctrl-C to stop it.";
        case Stopped = "Server has stopped.";
        case TimeOut = "Time out, accepting connection again.";
        case Error = "Could not create a server socket. Exiting with error.";
    }

    interface Service
    {
        public function run(Socket $connection): void;
    }

    final class EchoService implements Service
    {
        public const READ_BUFFER = 4096;

        /**
         * Implement the echo functionality of writing to a socket what it read from it
         * @param Socket $connection
         * @return void
         */
        public function run(Socket $connection): void
        {
            while ($input = socket_read($connection, self::READ_BUFFER)) {
                socket_write($connection, $input);
            }
            socket_close($connection);
        }
    }

    final class TCPServer
    {
        /**
         * @param Socket $socket
         */
        private function __construct(public readonly Socket $socket)
        {
        }

        /**
         * Create and bind the socket
         * @param string $address
         * @param int $port
         * @return TCPServer
         */
        public static function create(string $address, int $port): TCPServer
        {
            if (! $socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP)) {
                echo Status::Error->value . PHP_EOL;
                exit(EXIT_WITH_ERROR);
            }
            if (! socket_bind($socket, $address,$port)) {
                echo Status::Error->value . PHP_EOL;
                exit(EXIT_WITH_ERROR);
            }
            echo Status::Started->value . PHP_EOL;
            return new self($socket);
        }

        /**
         * Listen to and continuously accept incoming connection
         * @param TCPServer $server
         * @param Service $service
         * @return void
         */
        public static function listen(TCPServer $server, Service $service): void
        {
            if (! socket_listen($server->socket,1)) {
                echo Status::Error->value . PHP_EOL;
                exit(EXIT_WITH_ERROR);
            }
            while (true) {
                while ($conn = socket_accept($server->socket)) {
                    $service->run($conn);
                }
                echo Status::TimeOut->value . PHP_EOL;
            }
        }
    }

    $server = TCPServer::create("0.0.0.0",7);
    TCPServer::listen($server, new EchoService());
